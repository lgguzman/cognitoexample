package com.example.lgguzman.cognitoexample.notifications;

import android.content.SharedPreferences;
import android.util.Log;

import com.example.lgguzman.cognitoexample.cognito.CognitoSyncClientManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static android.content.ContentValues.TAG;

/**
 * Created by lgguzman on 13/01/17.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        CognitoSyncClientManager.setToken(refreshedToken, getApplicationContext());
       // CognitoSyncClientManager.sendTokenAWS(getApplicationContext());

        // TODO: Implement this method to send any registration to your app's servers.
       // sendRegistrationToServer(refreshedToken);
    }
}
