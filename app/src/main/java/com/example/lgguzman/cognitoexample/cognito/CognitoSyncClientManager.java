package com.example.lgguzman.cognitoexample.cognito;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.auth.AWSAbstractCognitoIdentityProvider;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.auth.CognitoCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lgguzman on 13/01/17.
 */

public class CognitoSyncClientManager {

    private static final String TAG = "CognitoSyncClientManager";

    /**
     * Enter here the Identity Pool associated with your app and the AWS
     * region where it belongs. Get this information from the AWS console.
     */

    public static final String IDENTITY_POOL_ID = "us-east-1:1dab8862-e813-4499-832e-d4ea4d1aee29";
    public static final String APP_ARN = "arn:aws:sns:us-east-1:740603019777:app/GCM/cognitoexample";
    public static final Regions REGION = Regions.US_EAST_1;

    private static CognitoSyncManager syncClient;
    protected static CognitoCachingCredentialsProvider credentialsProvider = null;
    protected static AWSAbstractCognitoIdentityProvider developerIdentityProvider;

    /**
     * Set this flag to true for using developer authenticated identities
     * Make sure you configured it in DeveloperAuthenticationProvider.java.
     */
    private static boolean useDeveloperAuthenticatedIdentities = false;


    /**
     * Initializes the Cognito Identity and Sync clients. This must be called before getInstance().
     *
     * @param context a context of the app
     */
    public static void init(Context context) {

        if (syncClient != null) return;
            credentialsProvider = new CognitoCachingCredentialsProvider(context, IDENTITY_POOL_ID,
                    REGION);
        syncClient = new CognitoSyncManager(context, REGION, credentialsProvider);
    }

    /**
     * Sets the login so that you can use authorized identity. This requires a
     * network request, so you should call it in a background thread.
     *
     * @param providerName the name of the external identity provider
     * @param token openId token
     */
    public static void addLogins(String providerName, String token) {


        Map<String, String> logins = credentialsProvider.getLogins();
        if (logins == null) {
            logins = new HashMap<String, String>();
        }
        logins.put(providerName,token);
        credentialsProvider.setLogins(logins);
    }

    /**
     * Gets the singleton instance of the CognitoClient. init() must be called
     * prior to this.
     *
     * @return an instance of CognitoClient
     */
    public static CognitoSyncManager getInstance() {

        return syncClient;
    }

    /**
     * Returns a credentials provider object
     *
     * @return
     */
    public static CognitoCredentialsProvider getCredentialsProvider() {
        if (credentialsProvider == null) {
            throw new IllegalStateException("CognitoSyncClientManager not initialized yet");
        }
        return credentialsProvider;
    }


    public static void setToken(String token, Context context){
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName(), 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("DeviceToken", token);
        editor.commit();
    }

    public static String getToken(Context context){
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName(), 0);
        return settings.getString("DeviceToken", "");
    }

    public static void sendTokenAWS(Context context) {

        final String regToken = getToken(context);
        if(!TextUtils.isEmpty(regToken) && credentialsProvider!=null) {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {


                    AmazonSNSClient pushClient = new AmazonSNSClient(credentialsProvider);

                    CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
                    platformEndpointRequest.setToken(regToken);
                    platformEndpointRequest.setPlatformApplicationArn(APP_ARN);
                    pushClient.setRegion(com.amazonaws.regions.Region.getRegion(REGION));

                    CreatePlatformEndpointResult result = pushClient.createPlatformEndpoint(platformEndpointRequest);
                    Log.e("Registration result", result.toString());

                    return null;
                }
            }.execute();
        }
    }

    }