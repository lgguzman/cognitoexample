package com.example.lgguzman.cognitoexample;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ForgotPasswordContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.NewPasswordContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler;
import com.example.lgguzman.cognitoexample.cognito.AppHelper;
import com.example.lgguzman.cognitoexample.cognito.CognitoSyncClientManager;
import com.example.lgguzman.cognitoexample.databinding.UserBinding;
import com.example.lgguzman.cognitoexample.model.User;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    private AlertDialog userDialog;
    private ProgressDialog waitDialog;

    User user=new User();
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        UserBinding binding = DataBindingUtil.<UserBinding>setContentView(this,R.layout.activity_main);
        binding.setUser(user);
        AppHelper.init(getApplicationContext());
        CognitoSyncClientManager.init(getApplicationContext());
        findCurrent();

    }


    private void findCurrent() {
        CognitoUser userC = AppHelper.getPool().getCurrentUser();
        username = userC.getUserId();
        if(username != null) {
            Log.e("Signin","no es null");
            AppHelper.setUser(username);
            user.setEmail(username);
            userC.getSessionInBackground(authenticationHandler);
        }else{
            Log.e("Signin","es null");
        }
    }

    public void click(View v) {

        switch (v.getId()){
            case R.id.sign_in:{
                SignUp();
                break;
            }
            case R.id.log_in:{
                break;
            }
            case R.id.anonymous:{
                break;
            }

        }

    }


    private void SignUp(){
        showWaitDialog("Signing up...");
        CognitoUserAttributes userAttributes = new CognitoUserAttributes();
        userAttributes.addAttribute("email", user.getEmail());
        AppHelper.getPool().signUpInBackground(user.getEmail(), user.getPassword(), userAttributes, null, signUpHandler);

    }

    GenericHandler confHandler = new GenericHandler() {
        @Override
        public void onSuccess() {
            if(AppHelper.getPool().getCurrentUser()!=null) {
                AppHelper.getPool().getCurrentUser().getSessionInBackground(authenticationHandler);
            }else{
                AppHelper.getPool().getUser(user.getEmail()).getSessionInBackground(authenticationHandler);
            }
        }

        @Override
        public void onFailure(Exception exception) {
            Toast.makeText(MainActivity.this,"errorr"+ exception.getMessage(),Toast.LENGTH_LONG).show();
        }
    };



    SignUpHandler signUpHandler = new SignUpHandler() {
        @Override
        public void onSuccess(CognitoUser user2, boolean signUpConfirmationState,
                              CognitoUserCodeDeliveryDetails cognitoUserCodeDeliveryDetails) {
            // Check signUpConfirmationState to see if the user is already confirmed
            closeWaitDialog();
            Boolean regState = signUpConfirmationState;
            if (signUpConfirmationState) {
                // User is already confirmed
                user2.getSessionInBackground(authenticationHandler);
            }
            else {

                createConfirmDialog(user2);

            }
        }

        @Override
        public void onFailure(Exception exception) {
            closeWaitDialog();
            Toast.makeText(MainActivity.this,"errorr2"+ exception.getMessage(),Toast.LENGTH_LONG).show();
        }
    };


    AuthenticationHandler authenticationHandler = new AuthenticationHandler() {
        @Override
        public void onSuccess(CognitoUserSession cognitoUserSession, CognitoDevice device) {
           // Log.e(TAG, "Auth Success");
            AppHelper.setCurrSession(cognitoUserSession);
            AppHelper.newDevice(device);
            String idToken = cognitoUserSession.getIdToken().getJWTToken();
            Toast.makeText(MainActivity.this,"el  token es!"+idToken,Toast.LENGTH_LONG).show();
            Log.e("POOLSTRING","cognito-idp."+"us-east-1"+".amazonaws.com/"+AppHelper.USERID);
            CognitoSyncClientManager.addLogins("cognito-idp."+"us-east-1"+".amazonaws.com/"+AppHelper.USERID,idToken);
            new CountDownTimer(1000, 1000) {

                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                   CognitoSyncClientManager.sendTokenAWS(getApplicationContext());
                }
            }.start();

            Toast.makeText(MainActivity.this,"Handler successful!",Toast.LENGTH_LONG).show();

            closeWaitDialog();
           // launchUser();
        }

        @Override
        public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String username) {

            Locale.setDefault(Locale.US);
            Toast.makeText(MainActivity.this,"getAuthDetal",Toast.LENGTH_LONG).show();
         //   getUserAuthentication(authenticationContinuation, username);
            AuthenticationDetails authenticationDetails = new AuthenticationDetails(user.getEmail(), user.getPassword(), null);
            authenticationContinuation.setAuthenticationDetails(authenticationDetails);
            authenticationContinuation.continueTask();
            closeWaitDialog();
        }

        @Override
        public void getMFACode(MultiFactorAuthenticationContinuation multiFactorAuthenticationContinuation) {

            Toast.makeText(MainActivity.this,"MFACode!",Toast.LENGTH_LONG).show();
            closeWaitDialog();
           // mfaAuth(multiFactorAuthenticationContinuation);
        }

        @Override
        public void onFailure(Exception e) {

            Toast.makeText(MainActivity.this,"Handler Failure!"+e.getMessage(),Toast.LENGTH_LONG).show();
            closeWaitDialog();
        }

        @Override
        public void authenticationChallenge(ChallengeContinuation continuation) {
            Toast.makeText(MainActivity.this,"AuthChallenge!",Toast.LENGTH_LONG).show();
            closeWaitDialog();

        }
    };

    private void showWaitDialog(String message) {
        closeWaitDialog();
        waitDialog = new ProgressDialog(this);
        waitDialog.setTitle(message);
        waitDialog.show();
    }

    private void showDialogMessage(String title, String body) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(body).setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    userDialog.dismiss();
                } catch (Exception e) {
                    //
                }
            }
        });
        userDialog = builder.create();
        userDialog.show();
    }

    private void closeWaitDialog() {
        try {
            waitDialog.dismiss();
        }
        catch (Exception e) {
            //
        }
    }


    public void createConfirmDialog(final CognitoUser user2){
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View promptsView = li.inflate(R.layout.custom, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text

                                user2.confirmSignUpInBackground(userInput.getText().toString(),true,confHandler);

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}
