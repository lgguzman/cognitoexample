package com.example.lgguzman.cognitoexample.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.lgguzman.cognitoexample.BR;

/**
 * Created by lgguzman on 13/01/17.
 */

public class User extends BaseObservable {


    private String email;
    private String password;
    private String key;


    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }
}
